from eve import Eve

settings = {'DOMAIN': {'people': {}}}

app = Eve(settings=settings)

if __name__ == '__main__':
  
  app.run(host='0.0.0.0',port='5000')
