# This file is a template, and might need editing before it works on your project.
FROM python:2.7

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY requirements.txt /usr/src/app/
#RUN pip install --no-cache-dir -r requirements.txt

RUN groupadd --gid 2000 node && useradd --uid 2000 --gid 2000 --shell /bin/bash$ user

RUN pip install --no-cache-dir -r requirements.txt

COPY app.py /usr/src/app

EXPOSE 5000

USER user

CMD ["python", "app.py"]
