#!/bin/bash

apt-get update && apt-get install -y -qq wget

wget -q https://mirror.openshift.com/pub/openshift-v4/clients/oc/4.1/linux/oc.tar.gz

apt-get install -y -qq tar && tar -xzf oc.tar.gz && rm -r oc.tar.gz

#./oc login --token=tgAQw26KyCpi-UdvAWeAVcBOosCS8zYeTg77UzCFcJg \
#--server=https://api.us-east-2.online-starter.openshift.com:6443 \
#--insecure-skip-tls-verify

./oc login --token=$OC_TOKEN --server=$OC_SERVER
./oc delete projects --all

sleep 60

./oc new-project ex2

./oc new-app -f ex2_tmpl.yaml

sleep 10
./oc get routes --output=custom-columns=HOST/PORT:spec.host --no-headers=true > address
